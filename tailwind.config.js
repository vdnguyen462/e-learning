/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ['./src/**/*.{js,jsx,ts,tsx}'],
    theme: {
        extend: {
            backgroundImage: {
                'admin-bg': "url('/src/assets/images/bg.png')",
                'sidebar-bg': "url('/src/assets/images/bg-sidebar.png')",
                colors: {
                    bgColor: '#f0f8ff',
                    textColor: '#252525',
                    colorGlobal: '#41b294',
                    colorSPrimary: '#f6ba35',
                    colorWhite: '#fff',
                },
                scale: {
                    120: '1.2',
                },
            },
            boxShadow: {
                shadowCustom: 'rgba(0, 0, 0, 0.1) 0px 4px 12px',
            },
        },
        plugins: [],
    },
};
