import { Badge, Card, Divider } from 'antd';
import Meta from 'antd/es/card/Meta';
import React from 'react';
import { NavLink } from 'react-router-dom';
import images from '../../assets/images/imgs';
import { CalendarIcon, ChartIcon, TagIcon, TimeIcon } from '../../components/Icons/Icons';

export default function ItemCourseList({ item }) {
    return (
        <NavLink to={`/detail/${item.maKhoaHoc}`}>
            <div className="w-full h-full shadow-shadowCustom">
                <Badge.Ribbon text="Yêu thích" placement="start" color="red">
                    <Card hoverable cover={<img alt="" src={item.hinhAnh} className="w-full h-48" />}>
                        <Meta title={item.moTa} />
                        <div className="flex justify-between mt-4 mb-8">
                            <span>
                                <TimeIcon />8 giờ
                            </span>
                            <span>
                                <CalendarIcon />4 tuần
                            </span>
                            <span>
                                <ChartIcon />
                                Tất cả
                            </span>
                        </div>
                        <Divider style={{ borderColor: 'gray' }} />
                        <div className="flex justify-between items-center">
                            <div className="flex justify-center item-center">
                                <img src={images.user} alt="" className="h-8 w-8 mr-2" />
                                <span>Tim Cock</span>
                            </div>
                            <div className="flex flex-col">
                                <span className="line-through">800.000vnd</span>
                                <span className="flex items-center">
                                    400.000vnd
                                    <TagIcon className="h-5 w-5" />
                                </span>
                            </div>
                        </div>
                    </Card>
                </Badge.Ribbon>
            </div>
        </NavLink>
    );
}
