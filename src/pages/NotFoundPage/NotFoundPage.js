import React from 'react';
import notPage from '../../assets/lottie/404.json';
import Lottie from 'lottie-react';

export default function NotFoundPage() {
    return (
        <div className="h-screen w-screen">
            <Lottie className="h-full w-full" animationData={notPage} />
        </div>
    );
}
