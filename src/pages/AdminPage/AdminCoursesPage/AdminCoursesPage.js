import { Dropdown, Space, Table } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { DownIcon } from '../../../components/Icons/Icons';
import ModalAddCourse from '../../../components/ModalAdmin/ModalCourse/ModalAddCourse';
import ModalEditCourse from '../../../components/ModalAdmin/ModalCourse/ModalEditCourse';
import ModalRegisterCourse from '../../../components/ModalAdmin/ModalCourse/ModalRegisterCourse/ModalRegisterCourse';
import ModalEditUserAdmin from '../../../components/ModalAdmin/ModalUserAdmin/ModalEditUserAdmin';
import { setLoadingOff, setLoadingOn } from '../../../react-toolkit/loadingSlice';
import { setSuccessOff, setSuccessOn } from '../../../react-toolkit/successNotifySlice';
import { setWarningOff, setWarningOn } from '../../../react-toolkit/warningNotifySlice';
import { adminServices } from '../../../services/adminServices';
import { localServ } from '../../../services/localService';
import { coursesColumns } from './utils';

export default function AdminCoursesPage() {
    const [courses, setCourses] = useState([]);
    const [showModalEditUser, setShowModalEditUser] = useState(false);
    const [searchInput, setSearchInput] = useState('');
    const [filterResult, setFilterResult] = useState([]);
    const [showModalAddCourse, setShowModalAddCourse] = useState(false);
    const [showModalEditCourse, setShowModalEditCourse] = useState(false);
    const [showRegisterModal, setShowRegisterModal] = useState(false);
    const [dataRegister, setDataRegister] = useState('');

    const [categoryCourses, setCategoryCourse] = useState([]);
    const [dataCourse, setDataCourse] = useState(null);

    const dispatch = useDispatch();

    const userInfo = localServ?.get();

    const navigate = useNavigate();

    const fetchData = () => {
        adminServices
            .getCourses()
            .then((res) => {
                let coursesArr = res.data.map((course, index) => {
                    return {
                        ...course,
                        stt: index + 1,
                        action: (
                            <Space>
                                <button
                                    onClick={() => {
                                        handleShowModalRegisterCourse(course.maKhoaHoc);
                                    }}
                                    className="py-[6px] px-3 bg-[#41b294] text-white rounded mr-1 hover:bg-red-300 transition-all"
                                >
                                    Ghi danh
                                </button>
                                <button
                                    onClick={() => {
                                        handleShowModalEditCourse(course);
                                    }}
                                    className="py-[6px] px-3 bg-[#ffc107] text-black rounded mr-1 hover:bg-[#e0a800] transition-all"
                                >
                                    Sửa
                                </button>
                                <button
                                    onClick={() => {
                                        handleDeleteCourse(course.maKhoaHoc);
                                    }}
                                    className="py-[6px] px-3 bg-[#dc3545] text-white rounded hover:bg-[#c82333] transition-all"
                                >
                                    Xóa
                                </button>
                            </Space>
                        ),
                    };
                });
                setCourses(coursesArr);
            })
            .catch((err) => {
                console.log(err);
            });

        adminServices
            .getCategoryCourses()
            .then((res) => {
                setCategoryCourse(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleLogout = () => {
        localServ.remove();
        navigate('/login');
    };

    const handleShowModalEditUser = () => {
        setShowModalEditUser(true);
    };

    const handleCancelEditUser = () => {
        setShowModalEditUser(false);
    };

    const handleDeleteCourse = (course) => {
        adminServices
            .deleteCourse(course)
            .then((res) => {
                dispatch(setSuccessOn('Xóa thành công'));
                setTimeout(() => {
                    dispatch(setSuccessOff());
                }, 2000);
                fetchData();
            })
            .catch((err) => {
                dispatch(setWarningOn(err.response.data));
                setTimeout(() => {
                    dispatch(setWarningOff());
                }, 2000);
            });
    };

    const handleShowModalAddCourse = () => {
        setShowModalAddCourse(true);
    };

    const handleCancelAddCourse = () => {
        setShowModalAddCourse(false);
    };

    const handleShowModalEditCourse = (course) => {
        setShowModalEditCourse(true);
        setDataCourse(course);
    };

    const handleCancelEditCourse = () => {
        setShowModalEditCourse(false);
        setDataCourse(null);
    };

    const itemRender = (page, type, originalElement) => {
        if (type === 'prev') {
            return <a>{`< Trước`}</a>;
        }
        if (type === 'next') {
            return <a>{`Sau >`}</a>;
        }

        if (type === 'page') {
            return <a>{page}</a>;
        }

        return originalElement;
    };

    const items = [
        {
            key: '1',
            label: (
                <button onClick={handleShowModalEditUser}>
                    <span>Cập nhật thông tin</span>
                </button>
            ),
        },

        {
            key: '2',
            label: <button onClick={handleLogout}>Đăng xuất</button>,
        },
    ];

    const handleShowModalRegisterCourse = (course) => {
        setShowRegisterModal(true);
        setDataRegister(course);
    };

    const handleCancelRegisterCourse = () => {
        setShowRegisterModal(false);
    };

    const handleSearchItem = (searchValue) => {
        setSearchInput(searchValue);

        if (searchInput != '') {
            const filterData = courses.filter((item) => {
                return Object.values(item.tenKhoaHoc).join('').toLowerCase().includes(searchInput.toLowerCase());
            });
            setFilterResult(filterData);
        } else {
            setFilterResult(courses);
        }
    };

    return (
        <>
            <div>
                <div className="flex justify-between items-center mb-5">
                    <div>
                        <button
                            onClick={handleShowModalAddCourse}
                            className="px-3 py-2 bg-[#41B294] text-white text-[16px] rounded hover:bg-red-400"
                        >
                            Thêm khóa học
                        </button>
                    </div>
                    <div className="flex-grow text-center">
                        <input
                            onChange={(e) => {
                                handleSearchItem(e.target.value);
                            }}
                            className="w-2/5 px-3 py-2 border border-[#ced4da] rounded-sm focus-visible:outline-none focus-visible:border-[#41B294]"
                            type="text"
                            placeholder="Nhập vào khóa học cần tìm"
                        />
                    </div>
                    <div className="flex items-center justify-center">
                        <span>Chào {userInfo?.hoTen},</span>
                        <Dropdown menu={{ items }} className="flex items-center ml-3 hover:cursor-pointer">
                            <a href="" onClick={(e) => e.preventDefault()}>
                                <Space>
                                    <img
                                        className="h-10 w-10 mr-1"
                                        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQxvR6T35D9iC_Nj3DC-0CndBzeSJWz7O2-cu6KdMgrdQ&s"
                                        alt=""
                                    />
                                    <DownIcon />
                                </Space>
                            </a>
                        </Dropdown>
                    </div>
                </div>
            </div>
            <Table
                columns={coursesColumns}
                dataSource={searchInput.length > 1 ? filterResult : courses}
                bordered
                pagination={{
                    defaultPageSize: 5,
                    showSizeChanger: false,
                    itemRender: itemRender,
                    position: ['bottomCenter'],
                }}
            />

            <ModalRegisterCourse
                open={showRegisterModal}
                handleCancel={handleCancelRegisterCourse}
                maKhoaHoc={dataRegister}
            />

            <ModalAddCourse
                open={showModalAddCourse}
                handleCancel={handleCancelAddCourse}
                category={categoryCourses}
                fetchData={fetchData}
            />

            <ModalEditCourse
                open={showModalEditCourse}
                handleCancel={handleCancelEditCourse}
                category={categoryCourses}
                fetchData={fetchData}
                dataCourse={dataCourse}
            />
            <ModalEditUserAdmin open={showModalEditUser} handleCancel={handleCancelEditUser} userInfo={userInfo} />
        </>
    );
}
