import { Space } from 'antd';
import { GearIcon } from '../../../components/Icons/Icons';

export const coursesColumns = [
    {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        align: 'center',
    },
    {
        title: 'Mã khóa học',
        dataIndex: 'maKhoaHoc',
        key: 'maKhoaHoc',
        align: 'center',
    },
    {
        title: 'Tên khóa học',
        dataIndex: 'tenKhoaHoc',
        key: 'maLoaiNguoiDung',
        align: 'center',
    },
    {
        title: 'Hình ảnh',
        dataIndex: 'hinhAnh',
        key: 'hinhAnh',
        render: (_, { hinhAnh }) => {
            return (
                <div className="flex items-center justify-center">
                    <img className="w-auto h-[36px]" src={hinhAnh} alt="" />
                </div>
            );
        },
    },
    {
        title: 'Lượt xem',
        dataIndex: 'luotXem',
        key: 'luotXem',
        align: 'center',
    },

    {
        title: 'Người tạo',
        dataIndex: 'nguoiTao',
        key: 'nguoiTao',
        align: 'center',

        render: (_, { nguoiTao }) => {
            return <span>{nguoiTao.hoTen}</span>;
        },
    },

    {
        title: (
            <Space>
                <GearIcon />
            </Space>
        ),
        dataIndex: 'action',
        key: '',
        align: 'center',
    },
];
