import { Space, Table, Dropdown } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { adminServices } from '../../../services/adminServices';
import { userColumns } from './utils';
import { setWarningOff, setWarningOn } from '../../../react-toolkit/warningNotifySlice';
import { setSuccessOff, setSuccessOn } from '../../../react-toolkit/successNotifySlice';
import { useNavigate } from 'react-router-dom';
import { localServ } from '../../../services/localService';
import { DownIcon } from '../../../components/Icons/Icons';
import ModalUpdateUserAdmin from '../../../components/ModalAdmin/ModalUserAdmin/ModalUpdateUserAdmin';
import RegisterModalUserAdmin from '../../../components/ModalAdmin/ModalUserAdmin/RegisterModalUserAdmin/RegisterModalUserAdmin';
import ModalEditUserAdmin from '../../../components/ModalAdmin/ModalUserAdmin/ModalEditUserAdmin';
import ModalAddUserAdmin from '../../../components/ModalAdmin/ModalUserAdmin/ModalAddUserAdmin';
import '../admin.css';

export default function AdminUsersPage() {
    const [taiKhoan, setTaiKhoan] = useState('');
    const [users, setUsers] = useState([]);
    const [dataUser, setDataUser] = useState({});
    const [showModalUpdateUser, setShowModalUpdateUser] = useState(false);
    const [showRegisterModal, setShowRegisterModal] = useState(false);
    const [isModalOpenAddUser, setIsModalOpenAddUser] = useState(false);
    const [isModalOpenEditUser, setIsModalOpenEditUser] = useState(false);
    const [searchInput, setSearchInput] = useState('');
    const [filterResult, setFilterResult] = useState([]);
    const userInfo = localServ.get();

    const navigate = useNavigate();
    const dispatch = useDispatch();

    const handleShowRegisterModal = (user) => {
        setShowRegisterModal(true);
        setTaiKhoan(user);
    };

    const handleCancelRegisterModal = () => {
        setShowRegisterModal(false);
    };

    const handleShowModalUpdateUser = (user) => {
        setShowModalUpdateUser(true);
        setDataUser(user);
    };

    const handleCancelUpdateUser = () => {
        setShowModalUpdateUser(false);
    };

    const itemRender = (page, type, originalElement) => {
        if (type === 'prev') {
            return <a>{`< Trước`}</a>;
        }
        if (type === 'next') {
            return <a>{`Sau >`}</a>;
        }

        if (type === 'page') {
            return <a>{page}</a>;
        }

        return originalElement;
    };

    let handleDeleteUser = (user) => {
        adminServices
            .deleteUser(user)
            .then((res) => {
                fetchData();
                dispatch(setSuccessOn('Đã xóa thành công'));
                setTimeout(() => {
                    dispatch(setSuccessOff());
                }, 2000);
            })
            .catch((err) => {
                dispatch(setWarningOn(err.response.data));
                setTimeout(() => {
                    dispatch(setWarningOff());
                }, 2000);
                console.log(err.response.data);
            });
    };

    const showModalAddUser = () => {
        setIsModalOpenAddUser(true);
    };

    const handleCancelAddUser = () => {
        setIsModalOpenAddUser(false);
    };

    const showModalEditUser = () => {
        setIsModalOpenEditUser(true);
    };

    const handleCancelEditUser = () => {
        setIsModalOpenEditUser(false);
    };

    const handleLogout = () => {
        localServ.remove();
        navigate('/login');
    };

    const handleSearchItem = (searchValue) => {
        setSearchInput(searchValue);

        if (searchInput != '') {
            const filterData = users.filter((item) => {
                return (
                    Object.values(item.taiKhoan).join('').toLowerCase().includes(searchInput.toLowerCase()) ||
                    Object.values(item.hoTen).join('').toLowerCase().includes(searchInput.toLowerCase())
                );
            });
            setFilterResult(filterData);
        } else {
            setFilterResult(users);
        }
    };

    const items = [
        {
            key: '1',
            label: (
                <button onClick={showModalEditUser}>
                    <span>Cập nhật thông tin</span>
                </button>
            ),
        },

        {
            key: '2',
            label: <button onClick={handleLogout}>Đăng xuất</button>,
        },
    ];

    const fetchData = () => {
        adminServices
            .getUsers()
            .then((res) => {
                let usersArr = res.data.map((user, index) => {
                    return {
                        ...user,
                        stt: index + 1,
                        action: (
                            <Space>
                                <button
                                    onClick={() => {
                                        handleShowRegisterModal(user.taiKhoan);
                                    }}
                                    className="py-[6px] min-w-[92px] px-3 bg-[#41b294] text-white rounded mr-1 hover:bg-red-300 transition-all"
                                >
                                    Ghi danh
                                </button>
                                <button
                                    onClick={() => {
                                        handleShowModalUpdateUser(user);
                                    }}
                                    className="py-[6px] px-3 bg-[#ffc107] text-black rounded mr-1 hover:bg-[#e0a800] transition-all"
                                >
                                    Sửa
                                </button>
                                <button
                                    onClick={() => {
                                        handleDeleteUser(user.taiKhoan);
                                    }}
                                    className="py-[6px] px-3 bg-[#dc3545] text-white rounded hover:bg-[#c82333] transition-all"
                                >
                                    Xóa
                                </button>
                            </Space>
                        ),
                    };
                });
                setUsers(usersArr);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            <div className="flex justify-between items-center mb-5">
                <div>
                    <button
                        onClick={showModalAddUser}
                        className="px-3 py-2 bg-[#41B294] text-white text-[16px] rounded hover:bg-red-400"
                    >
                        Thêm tài khoản
                    </button>
                </div>
                <div className="flex-grow text-center">
                    <input
                        className="w-1/2 px-3 py-2 border border-[#ced4da] rounded-sm focus-visible:outline-none focus-visible:border-[#41B294]"
                        type="text"
                        placeholder="Nhập vào tài khoản hoặc họ tên người dùng"
                        onChange={(e) => {
                            handleSearchItem(e.target.value);
                        }}
                    />
                </div>
                <div className="flex items-center justify-center">
                    <span>Chào {userInfo?.hoTen},</span>
                    <Dropdown menu={{ items }} className="flex items-center ml-3 hover:cursor-pointer">
                        <a href="" onClick={(e) => e.preventDefault()}>
                            <Space>
                                <img
                                    className="h-10 w-10 mr-1"
                                    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQxvR6T35D9iC_Nj3DC-0CndBzeSJWz7O2-cu6KdMgrdQ&s"
                                    alt=""
                                />
                                <DownIcon />
                            </Space>
                        </a>
                    </Dropdown>
                </div>
            </div>

            <Table
                columns={userColumns}
                dataSource={searchInput.length > 1 ? filterResult : users}
                bordered
                pagination={{
                    defaultPageSize: 5,
                    showSizeChanger: false,
                    itemRender: itemRender,
                    position: ['bottomCenter'],
                }}
            />
            <ModalUpdateUserAdmin
                open={showModalUpdateUser}
                handleCancel={handleCancelUpdateUser}
                userInfo={dataUser}
                fetchData={fetchData}
            />
            <RegisterModalUserAdmin
                open={showRegisterModal}
                handleCancel={handleCancelRegisterModal}
                taiKhoan={taiKhoan}
            />

            <ModalAddUserAdmin open={isModalOpenAddUser} handleCancel={handleCancelAddUser} fetchData={fetchData} />
            <ModalEditUserAdmin open={isModalOpenEditUser} handleCancel={handleCancelEditUser} userInfo={userInfo} />
        </>
    );
}
