import { Space } from 'antd';
import { GearIcon } from '../../../components/Icons/Icons';

export const userColumns = [
    {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
        align: 'center',
    },
    {
        title: 'Tài khoản',
        dataIndex: 'taiKhoan',
        key: 'taiKhoan',
        align: 'center',
    },
    {
        title: 'Người dùng',
        dataIndex: 'maLoaiNguoiDung',
        key: 'maLoaiNguoiDung',
        align: 'center',
    },
    {
        title: 'Họ và tên',
        dataIndex: 'hoTen',
        key: 'hoTen',
        align: 'center',
    },
    {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
        align: 'center',
    },
    {
        title: 'Số điện thoại',
        dataIndex: 'soDt',
        key: 'soDt',
        align: 'center',
    },
    {
        title: (
            <Space>
                {' '}
                <GearIcon />
            </Space>
        ),
        dataIndex: 'action',
        key: '',
        align: 'center',
    },
];
