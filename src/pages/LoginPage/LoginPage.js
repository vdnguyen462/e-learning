import { Button, Checkbox, Form, Input, message } from 'antd';
import { useEffect } from 'react';
import { userServ } from '../../services/userService';
import { NavLink, Navigate, useNavigate } from 'react-router-dom';
import { setSuccessOff, setSuccessOn } from '../../react-toolkit/successNotifySlice';
import { useDispatch } from 'react-redux';
import { localServ } from '../../services/localService';
import { userInfor } from '../../react-toolkit/userLoginSlice';
import { setLoginWarningOff, setLoginWarningOn } from '../../react-toolkit/LoginWarningNotifySlice';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footter';

const LoginPage = () => {
    let navigate = useNavigate();
    let dispatch = useDispatch();

    const onFinish = (values) => {
        userServ
            .postLogin(values)
            .then((res) => {
                localServ.set(res.data);
                setTimeout(() => {
                    navigate('/home');
                    dispatch(setSuccessOff());
                }, 2000);
                dispatch(setSuccessOn('đăng nhập'));
                dispatch(userInfor(res.data));
            })
            .catch((err) => {
                console.log(err);
                dispatch(setLoginWarningOn());
                setTimeout(() => {
                    dispatch(setLoginWarningOff());
                }, 2000);
            });
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <>
            <Header />
            <div className="flex flex-col items-center justify-center px-96 py-10 mx-auto md:h-screen lg:py-0">
                <h1 class="text-xl font-bold mb-6 leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-black">
                    Sign in to your account
                </h1>
                <Form
                    layout="vertical"
                    className="px-36 form-login shadow-xl rounded-xl"
                    name="basic"
                    labelCol={{
                        span: 8,
                    }}
                    wrapperCol={{
                        span: 24,
                    }}
                    style={{
                        width: '100%',
                    }}
                    initialValues={{
                        remember: true,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <Form.Item
                        className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                        label="Tài Khoản"
                        name="taiKhoan"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your username!',
                            },
                        ]}
                    >
                        <Input
                            placeholder="Enter Username"
                            className="bg-blue-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-600 focus:border-blue-600 block w-full p-2.5 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                        />
                    </Form.Item>

                    <Form.Item
                        className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                        label="Mật Khẩu"
                        name="matKhau"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your password!',
                            },
                        ]}
                    >
                        <Input.Password
                            placeholder="Enter Password"
                            className="bg-gray-50 border flex border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-600 focus:border-blue-600 w-full p-2.5 dark:border-gray-600 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                        />
                    </Form.Item>

                    <Form.Item
                        className="text-right "
                        wrapperCol={{
                            span: 24,
                        }}
                    >
                        <Button
                            className="w-full text-white bg-blue-600 hover:bg-blue-700 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-xl px- pb-5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                            htmlType="submit"
                        >
                            Đăng Nhập
                        </Button>
                    </Form.Item>
                    <Form.Item className="text-base text-right hover:text-black">
                        <NavLink to="/register" className="hover:text-black">
                            Bạn chưa có tài khản ? Đăng Ký
                        </NavLink>
                    </Form.Item>
                </Form>
            </div>
            <Footer />
        </>
    );
};
export default LoginPage;
