import React from 'react';
import underConstruction from '../../assets/lottie/underConstruction.json';
import Lottie from 'lottie-react';

export default function UnderConstruction() {
    return (
        <div>
            <div className="h-screen w-screen">
                <Lottie className="h-full w-full" animationData={underConstruction} />
            </div>
        </div>
    );
}
