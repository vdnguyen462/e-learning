import React from 'react';
import './ReviewPage.css';

export default function ReviewPage() {
    return (
        <div className="review mt-5">
            <div className="reviewStudent">
                <div className="triangleTopRight"></div>
                <div className="smallBox smallboxLeftTop"></div>
                <div className="smallBox smallboxRightTop"></div>
                <div className="smallBox smallboxRightBottom"></div>
                <div className="smallBox smallboxRightBottom"></div>
                <div className="grid grid-flow-row grid-cols-2">
                    <div>
                        <div className="reviewImg">
                            <div className="bgStudentReview"></div>
                            <img src="./image/avatarReview.2f5a1f3c.png" alt="" />
                        </div>
                    </div>
                    <div className="quoteRight">
                        <blockquote className="textQuote">
                            <p>
                                Chương trình giảng dạy được biên soạn dành riêng cho các bạn Lập trình từ trái ngành
                                hoặc đã có kiến thức theo cường độ cao, luôn được tinh chỉnh và tối ưu hóa theo thời
                                gian bởi các thành viên sáng lập và giảng viên dày kinh nghiệm.Thực sự rất hay và hấp
                                dẫn
                            </p>
                        </blockquote>
                        <p className="textName">Nhi Dev</p>
                        <span>Học viên xuất sắc</span>
                    </div>
                </div>
            </div>
        </div>
    );
}
