import React, { useEffect, useState } from 'react';
import Tippy from '@tippyjs/react/headless';
import { adminServices } from '../../services/adminServices';
import { useDispatch } from 'react-redux';
import { setSuccessOff, setSuccessOn } from '../../react-toolkit/successNotifySlice';
import { setWarningOn, setWarningOff } from '../../react-toolkit/warningNotifySlice';

export default function SearchCourse({ taiKhoan, fetchApi }) {
    const [listCourses, setListCourses] = useState([]);
    const [filterResult, setFilterResult] = useState([]);
    const [searchInput, setSearchInput] = useState('');
    const [userInfo, setUserInfo] = useState({
        taiKhoan: taiKhoan,
        maKhoaHoc: null,
    });
    const [show, setShow] = useState(false);
    const dispatch = useDispatch();

    useEffect(() => {
        getDataAPI();
    }, [taiKhoan]);

    const getDataAPI = () => {
        adminServices
            .getListCourses(taiKhoan)
            .then((res) => {
                setListCourses(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const searchItems = (searchValue) => {
        setSearchInput(searchValue);

        if (searchInput != '') {
            const filterData = listCourses.filter((item) => {
                return Object.values(item.tenKhoaHoc).join('').toLowerCase().includes(searchInput.toLowerCase());
            });
            setFilterResult(filterData);
        } else {
            setFilterResult(listCourses);
        }
    };

    const handleShow = () => {
        setShow(true);
    };

    const handleHide = () => {
        setShow(false);
    };

    const handleOnClick = (e) => {
        setSearchInput(e.target.innerText);
        setUserInfo({ taiKhoan: taiKhoan, maKhoaHoc: e.target.dataset.makhoahoc });
        setShow(false);
    };

    const handleRegisterCourse = () => {
        adminServices
            .registerCourse(userInfo)
            .then((res) => {
                dispatch(setSuccessOn('Đã ghi danh thành công'));
                setTimeout(() => {
                    dispatch(setSuccessOff());
                }, 2000);
                setSearchInput('');
                setUserInfo({ taiKhoan: taiKhoan, maKhoaHoc: null });
                getDataAPI();
                fetchApi();
            })
            .catch((err) => {
                dispatch(setWarningOn('Vui lòng chọn khóa học'));
                setTimeout(() => {
                    dispatch(setWarningOff());
                }, 2000);
                console.log(err.response.data);
            });
    };
    return (
        <>
            <div>
                <Tippy
                    offset={(0, 0)}
                    placement={'bottom-start'}
                    interactive
                    visible={searchItems && show}
                    onClickOutside={handleHide}
                    render={(attrs) => (
                        <div
                            className="flex flex-col border border-[#ced4da] bg-white max-w-[300px] rounded`"
                            tabIndex="-1"
                            {...attrs}
                        >
                            <div className="max-h-80 overflow-scroll overflow-x-hidden">
                                {searchInput.length > 1
                                    ? filterResult.map((item) => {
                                          return (
                                              <div
                                                  data-makhoahoc={item.maKhoaHoc}
                                                  onClick={handleOnClick}
                                                  key={item.maKhoaHoc}
                                                  className="hover:bg-[#ede6e6ce] px-3 py-2 hover:cursor-pointer"
                                              >
                                                  {item.tenKhoaHoc}
                                              </div>
                                          );
                                      })
                                    : listCourses.map((item) => {
                                          return (
                                              <div
                                                  data-makhoahoc={item.maKhoaHoc}
                                                  onClick={handleOnClick}
                                                  key={item.maKhoaHoc}
                                                  className="hover:bg-[#ede6e6ce] px-3 py-2 hover:cursor-pointer"
                                              >
                                                  {item.tenKhoaHoc}
                                              </div>
                                          );
                                      })}
                            </div>
                        </div>
                    )}
                >
                    <input
                        value={searchInput}
                        onFocus={handleShow}
                        onChange={(e) => searchItems(e.target.value)}
                        className="w-[300px] focus-visible:outline-0 border border-[#ced4da] rounded px-3 py-[6px] focus:border-red-400"
                        type="text"
                        placeholder="Chọn khóa học"
                    />
                </Tippy>
            </div>
            <button
                onClick={handleRegisterCourse}
                className="px-3 py-[6px] bg-[#41B294] rounded text-white hover:bg-red-400"
            >
                Ghi danh
            </button>
        </>
    );
}
