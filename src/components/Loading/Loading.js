import Lottie from 'lottie-react';
import React from 'react';
import { useSelector } from 'react-redux';
import loginAnimation from '../../assets/lottie/loading.json';
export default function Loading() {
    const { isLoading } = useSelector((state) => state.loadingSlice);

    return isLoading ? (
        <div className="h-screen w-screen top-0 left-0 z-50 grid items-center justify-center fixed bg-[#9994]">
            <Lottie animationData={loginAnimation} />
        </div>
    ) : (
        <></>
    );
}
