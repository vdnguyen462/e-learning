import { Divider, Modal, Space, Table } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { setSuccessOff, setSuccessOn } from '../../../../react-toolkit/successNotifySlice';
import { adminServices } from '../../../../services/adminServices';
import SearchCourse from '../../../Search/SearchCourses';
import { approvedCourse, waitingCourse } from './utils';

export default function RegisterModalUserAdmin({ open, handleCancel, taiKhoan }) {
    const [waitingCourseList, setWaitingCourseList] = useState([]);
    const [approvedCourseList, setApprovedCourseList] = useState([]);
    const dispatch = useDispatch();

    let userInfo = { taiKhoan };

    const fetchApi = () => {
        adminServices
            .getWaitingCourseList(userInfo)
            .then((res) => {
                let newValues = res.data.map((item, index) => {
                    return {
                        ...item,
                        stt: index + 1,
                        action: (
                            <Space>
                                <button
                                    onClick={() => {
                                        handleAuthCourse({ taiKhoan, maKhoaHoc: item.maKhoaHoc });
                                    }}
                                    className="px-3 py-[6px] bg-[#41B294] rounded text-white hover:bg-red-400"
                                >
                                    Xác thực
                                </button>
                                <button
                                    onClick={() => {
                                        handleCancelRegister({
                                            taiKhoan: taiKhoan,
                                            maKhoaHoc: item.maKhoaHoc,
                                        });
                                    }}
                                    className="px-3 py-[6px] bg-red-800 rounded text-white hover:bg-red-700"
                                >
                                    Xóa
                                </button>
                            </Space>
                        ),
                    };
                });

                setWaitingCourseList(newValues);
            })
            .catch((err) => {
                console.log(err);
            });

        adminServices
            .getApprovedCourseList(userInfo)
            .then((res) => {
                let newValue = res.data?.map((item, index) => {
                    return {
                        ...item,
                        stt: index + 1,
                        action: (
                            <Space>
                                <button
                                    onClick={() => {
                                        handleCancelRegister({
                                            taiKhoan: taiKhoan,
                                            maKhoaHoc: item.maKhoaHoc,
                                        });
                                    }}
                                    className="px-3 py-[6px] bg-red-800 rounded text-white hover:bg-red-700"
                                >
                                    Hủy
                                </button>
                            </Space>
                        ),
                    };
                });
                setApprovedCourseList(newValue);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const handleCancelRegister = (dataCourse) => {
        adminServices
            .cancelRegister(dataCourse)
            .then((res) => {
                dispatch(setSuccessOn('Hủy thành công'));
                setTimeout(() => {
                    dispatch(setSuccessOff());
                }, 2000);
                fetchApi();
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const handleAuthCourse = (data) => {
        adminServices
            .registerCourse(data)
            .then((res) => {
                dispatch(setSuccessOn('Ghi danh thành công'));
                setTimeout(() => {
                    dispatch(setSuccessOff());
                }, 2000);
                fetchApi();
            })
            .catch((err) => {
                console.log(err);
            });
    };

    useEffect(() => {
        fetchApi();
    }, [taiKhoan]);

    return (
        <>
            <Modal
                width={800}
                height={400}
                open={open}
                onCancel={handleCancel}
                closable={false}
                footer={null}
                className="top-1"
            >
                <div>
                    <div className="flex justify-between items-center">
                        <span className="font-semibold text-xl">Chọn khóa học</span>
                        <SearchCourse taiKhoan={taiKhoan} fetchApi={fetchApi} />
                    </div>
                    <Divider style={{ borderColor: 'black', margin: '12px 0px' }} />
                    <div>
                        <span className="font-semibold text-xl">Khóa học chờ xác thực</span>
                        <Table
                            className="mt-3"
                            size="small"
                            columns={waitingCourse}
                            dataSource={waitingCourseList}
                            bordered
                            pagination={{
                                defaultPageSize: 2,
                                showSizeChanger: false,

                                position: ['bottomCenter'],
                            }}
                        ></Table>
                    </div>
                    <Divider style={{ borderColor: 'black' }} />
                    <div>
                        <span className="font-semibold text-xl">Khóa học đã ghi danh</span>
                        <Table
                            className="mt-3"
                            size="small"
                            columns={approvedCourse}
                            dataSource={approvedCourseList}
                            bordered
                            pagination={{
                                defaultPageSize: 2,
                                showSizeChanger: false,
                                position: ['bottomCenter'],
                            }}
                        ></Table>
                    </div>
                    <Divider style={{ borderColor: 'black' }} />
                    <div className="text-right">
                        <button
                            onClick={handleCancel}
                            className="px-3 py-[6px] rounded bg-red-700 text-white ml-2 hover:bg-red-800"
                        >
                            Đóng
                        </button>
                    </div>
                </div>
            </Modal>
        </>
    );
}
