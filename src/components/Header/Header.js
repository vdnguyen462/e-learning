import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import images from '../../assets/images/imgs';
import { courceServ } from '../../services/courseService';
import { localServ } from '../../services/localService';
import { GearIcon, PowerIcon } from '../Icons/Icons';
import './Header.css';

const Header = ({ data }) => {
    const [category, setCategory] = useState([]);
    const userInfo = localServ.get();

    useEffect(() => {
        courceServ
            .getCourseCategoty()
            .then((res) => {
                setCategory(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);
    let renderCateCourse = () => {
        return category.map((item, index) => {
            return (
                <li key={index}>
                    <NavLink to={`/danhmuckhoahoc/${item.maDanhMuc}`}>{item.tenDanhMuc}</NavLink>
                </li>
            );
        });
    };

    let renderShowMobie = () => {
        let menuMobie = document.querySelector('.menuHeaderMobie');
        if (menuMobie) {
            menuMobie.classList.toggle('active');
        } else {
            return '';
        }
    };

    const handleLogout = () => {
        localServ.remove();
        window.location.reload();
    };

    return (
        <div className="header headerFixed">
            <div className="headerLeft">
                <a href="/home" className="textLogo">
                    <img src={images.logo} alt="" width={250} />
                </a>
                <form>
                    <input action="#" className="searchForm" type="text" placeholder="Tìm kiếm" />
                </form>
            </div>
            <div className="headerRight">
                <ul className="menuHeader">
                    <li className="courseCate">
                        <i class="fas fa-bars mr-1"></i>
                        <NavLink to="/home">Danh mục</NavLink>
                        <ul className="courseCateList">{renderCateCourse()}</ul>
                    </li>
                    <li>
                        <NavLink to="/course">Khoá học</NavLink>
                    </li>
                    <li>
                        <NavLink to="/blog">Blog</NavLink>
                    </li>
                    <li className="eventHeader courseCate">
                        <NavLink to="/event">Sự kiện</NavLink>
                        <ul className="courseCateList">
                            <li>
                                <NavLink to="/sukien/lastYear">Sự kiện sale cuối năm</NavLink>
                            </li>
                            <li>
                                <NavLink to="/sukien/Noel">Sự kiện giáng sinh</NavLink>
                            </li>
                            <li>
                                <NavLink to="/sukien/Noel">Sự kiện Noel</NavLink>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <NavLink to="/information">Thông tin</NavLink>
                    </li>
                </ul>
            </div>
            <div className="showIconHeader">
                {userInfo == null ? (
                    <button className="btnGlobal">
                        <a href="/login">Đăng nhập</a>
                    </button>
                ) : (
                    <div className="flex justify-center items-center">
                        <NavLink to="/admin">
                            <GearIcon className="hover:cursor-pointer" />
                        </NavLink>
                        <NavLink to="/thong-tin-ca-nhan">
                            <img src={images.user} alt="" className="w-10 h-10 mx-2 hover:cursor-pointer" />
                        </NavLink>
                        <div onClick={handleLogout}>
                            <PowerIcon className="hover:cursor-pointer" />
                        </div>
                    </div>
                )}
                <div className="menuMobie">
                    <i class="fas fa-sort-down iconMenuMobie" onClick={renderShowMobie}></i>
                    <ul className="menuHeaderMobie">
                        <li>
                            <form action="">
                                <input type="text" className="searchFormMobie" placeholder="Tìm kiếm" />
                            </form>
                        </li>
                        <li className="courseCateMobie">
                            <NavLink to="/home">Danh mục</NavLink>
                            <ul className="courseCateListMobie">{renderCateCourse()}</ul>
                        </li>
                        <li>
                            <NavLink to="/course">Khoá học</NavLink>
                        </li>
                        <li>
                            <NavLink to="/blog">Blog</NavLink>
                        </li>
                        <li className="eventHeaderMobie courseCateMobie">
                            <NavLink to="/event">Sự kiện</NavLink>
                            <ul className="courseCateListMobie">
                                <li>
                                    <NavLink to="/home">Sự kiện sale cuối năm</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/home">Sự kiện giáng sinh</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/home">Sự kiện Noel</NavLink>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <NavLink to="/information">Thông tin</NavLink>
                        </li>
                        <li>
                            <NavLink to="/logout">Đăng xuất</NavLink>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default Header;
