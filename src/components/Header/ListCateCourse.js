import React, { useEffect, useState } from 'react';
import { courceServ } from '../../services/courseService';
import Header from './Header';

export default function ListCateCourse() {
    const [category, setCategory] = useState([]);
    useEffect(() => {
        courceServ
            .getCourseCategoty()
            .then((res) => {
                setCategory(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    return (
        <div>
            {category.map((item) => {
                return <Header data={item} />;
            })}
        </div>
    );
}
