import React from 'react';
import { HomeIcon, CourseIcon, UserIcon } from '../components/Icons/Icons';
import { Layout } from 'antd';
import { NavLink } from 'react-router-dom';
import SidebarAdmin from '../components/SidebarAdmin/SidebarAdmin';
const { Content } = Layout;

export default function AdminLayout({ Component }) {
    const getItems = (label, key, icon) => {
        return { label, key, icon };
    };

    const items = [
        getItems(<NavLink to="/home" />, '1', <HomeIcon />),
        getItems(<NavLink to="/admin/quanlynguoidung"></NavLink>, '2', <UserIcon />),
        getItems(<NavLink to="/admin/quanlykhoahoc"></NavLink>, '3', <CourseIcon />),
    ];

    return (
        <>
            <Layout className="p-5 bg-admin-bg h-screen flex-row">
                <SidebarAdmin />
                <Layout className="ml-1 bg-white rounded-lg px-2 py-4">
                    <Content>
                        <Component />
                    </Content>
                </Layout>
            </Layout>
        </>
    );
}
