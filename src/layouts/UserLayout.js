import React from 'react';
import Footer from '../components/Footer/Footter';
import Ct_BackTop from '../utils/BackTop/BackTop';
import Header from '../components/Header/Header';

export default function UserLayout({ Component }) {
    return (
        <>
            <Header />
            <Ct_BackTop />
            <Component />
            <Footer />
        </>
    );
}
