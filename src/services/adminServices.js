import { https } from './config';

export const adminServices = {
    getUsers: () => {
        return https.get('/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP01');
    },

    getCourses: () => {
        return https.get('/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01');
    },

    addUser: (userInfo) => {
        return https.post('/api/QuanLyNguoiDung/ThemNguoiDung', userInfo);
    },

    updateUser: (userInfo) => {
        return https.put('/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung', userInfo);
    },

    deleteUser: (user) => {
        return https.delete(`https://elearningnew.cybersoft.edu.vn/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${user}`);
    },

    getListCourses: (user) => {
        return https.post(`/api/QuanLyNguoiDung/LayDanhSachKhoaHocChuaGhiDanh?TaiKhoan=${user}`);
    },

    registerCourse: (data) => {
        return https.post('/api/QuanLyKhoaHoc/GhiDanhKhoaHoc', data);
    },

    getWaitingCourseList: (account) => {
        return https.post('/api/QuanLyNguoiDung/LayDanhSachKhoaHocChoXetDuyet', account);
    },

    getApprovedCourseList: (account) => {
        return https.post('/api/QuanLyNguoiDung/LayDanhSachKhoaHocDaXetDuyet', account);
    },

    cancelRegister: (data) => {
        return https.post('/api/QuanLyKhoaHoc/HuyGhiDanh', data);
    },

    deleteCourse: (course) => {
        return https.delete(`/api/QuanLyKhoaHoc/XoaKhoaHoc?MaKhoaHoc=${course}`);
    },

    getCategoryCourses: () => {
        return https.get('/api/QuanLyKhoaHoc/LayDanhMucKhoaHoc');
    },

    addCourse: (course) => {
        return https.post('/api/QuanLyKhoaHoc/ThemKhoaHoc', course);
    },

    updateCourse: (course) => {
        return https.put('/api/QuanLyKhoaHoc/CapNhatKhoaHoc', course);
    },

    getUnRegisterStudentList: (course) => {
        return https.post('/api/QuanLyNguoiDung/LayDanhSachNguoiDungChuaGhiDanh', course);
    },

    getRegisteredStudentList: (course) => {
        return https.post('/api/QuanLyNguoiDung/LayDanhSachHocVienKhoaHoc', course);
    },

    getWaitingRegisterStudentList: (course) => {
        return https.post('/api/QuanLyNguoiDung/LayDanhSachHocVienChoXetDuyet', course);
    },
};
