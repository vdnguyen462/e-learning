import { https } from './config';

export const courceServ = {
    getListCource: () => {
        return https.get('/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01');
    },
    getInforCource: (id) => {
        return https.get(`/api/QuanLyKhoaHoc/LayThongTinKhoaHoc?maKhoaHoc=${id}`);
    },
    getRegisterCourse: (course) => {
        return https.post('/api/QuanLyKhoaHoc/DangKyKhoaHoc', course);
    },
    getCourseList: (khoaHoc) => {
        return https.get(`/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?tenKhoaHoc=${khoaHoc}&MaNhom=GP01`);
    },
    getCourseCategoty: () => {
        return https.get('/api/QuanLyKhoaHoc/LayDanhMucKhoaHoc');
    },
    getCourseFullList: () => {
        return https.get('/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc');
    },
    getCourseListFollowCategory: (courseId) => {
        return https.get(`/api/QuanLyKhoaHoc/LayKhoaHocTheoDanhMuc?maDanhMuc=${courseId}&MaNhom=GP01`);
    },
};
