import './App.css';
import Loading from './components/Loading/Loading';
import SuccessNotify from './components/SuccessNotify/SuccessNotify';
import WarningNotify from './components/WarningNotify/WarningNotify';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import { routes } from './routes/routes';
import UserWarningNotify from './components/UserWarningNotify/UserWarningNotify';
import LoginWarningNotify from './components/LoginWarningNotifyy/LoginWarningNotify';

function App() {
    return (
        <>
            <Loading />
            <SuccessNotify />
            <WarningNotify />
            <UserWarningNotify />
            <BrowserRouter>
                <LoginWarningNotify />
                <Routes>
                    {routes?.map(({ path, component }) => {
                        return <Route path={path} element={component} key={path} />;
                    })}
                </Routes>
            </BrowserRouter>
        </>
    );
}

export default App;
