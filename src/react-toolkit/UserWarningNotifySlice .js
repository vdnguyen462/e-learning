import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    isUserWarning: false,
};

const UserWarningNotifySlice = createSlice({
    name: 'UserWarningNotifySlice',
    initialState,
    reducers: {
        setUserWarningOn: (state, action) => {
            state.isUserWarning = true;
        },
        setUserWarningOff: (state, action) => {
            state.isUserWarning = false;
        },
    },
});

export const { setUserWarningOn, setUserWarningOff } = UserWarningNotifySlice.actions;

export default UserWarningNotifySlice.reducer;
