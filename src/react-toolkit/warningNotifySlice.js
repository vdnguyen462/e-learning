import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    isWarning: false,
    messageWarning: '',
};

const warningNotifySlice = createSlice({
    name: 'warningNotifySlice',
    initialState,
    reducers: {
        setWarningOn: (state, action) => {
            state.isWarning = true;
            state.messageWarning = action.payload;
        },
        setWarningOff: (state, action) => {
            state.isWarning = false;
            state.messageWarning = '';
        },
    },
});

export const { setWarningOn, setWarningOff } = warningNotifySlice.actions;

export default warningNotifySlice.reducer;
