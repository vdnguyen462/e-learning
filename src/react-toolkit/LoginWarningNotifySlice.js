import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    isLoginWarning: false,
};

const LoginWarningNotifySlice = createSlice({
    name: 'loginWarningNotifySlice',
    initialState,
    reducers: {
        setLoginWarningOn: (state, action) => {
            state.isLoginWarning = true;
        },
        setLoginWarningOff: (state, action) => {
            state.isLoginWarning = false;
        },
    },
});

export const { setLoginWarningOn, setLoginWarningOff } = LoginWarningNotifySlice.actions;

export default LoginWarningNotifySlice.reducer;
